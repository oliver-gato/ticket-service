ticker-service is a programming challenge to demonstrate various Golang techniques related to concurrency.

Required version: go1.10.2 or higher.

Run tests from the command line when in /ticket-service directory:
go test ./...

The following functionality is the end goal of the service:

1. Find the number of seats available within the venue
Note: available seats are seats that are neither held nor reserved.

2. Find and hold the best available seats on behalf of a customer
Note: each ticket hold should expire within a set number of seconds.

3. Reserve and commit a specific group of held seats for a customer

TO DO:
-- record e-mail during hold and add email to the logic to verify reservation

-- increase test coverage in all modules

-- place hold on seats based on proximity (prefer seats that are in the same row for each request)

-- create index structure to allow efficient seat search operations (e.g. block index structure for each row)

-- create a web server and three resources that accept http requests

-- create a persistent transaction log of request for recovery purposes
