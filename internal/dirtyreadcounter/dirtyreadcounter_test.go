package dirtyreadcounter_test

import (
	"reflect"
	"testing"
	"ticket-service/internal/dirtyreadcounter"
	"ticket-service/internal/seatkeeper"
	"time"
)

func TestShouldInteractWithDirtyreadcounterRoutine1(t *testing.T) {
	// start a seatkeeper goroutine
	cb := seatkeeper.New(3, 5, 1, 1, 200*time.Millisecond)

	// start a dirtyreadcounter routine
	p := dirtyreadcounter.New(
		200*time.Millisecond, // this should be a few hundered milliseconds
		cb.QuitReceiveCh,
		cb.SeatCountSendCh,
		cb.FreeSeatsReceiveCh,
	)
	// call a method to get the counts from the mutex-protected struct
	approxSeatCounts := p.Get()

	// to improve this test, I want to make some modifications using seatkeeper channels
	// and get another approximate count after a short wait which should reflect the change

	//all routines must exit, this can only be called from the same location that called seatkeeper.New()
	close(cb.QuitSendCh)

	expectedSeatCounts := dirtyreadcounter.ApproxSeatCounts{Free: 15, Held: 0, Reserved: 0}
	if !reflect.DeepEqual(approxSeatCounts, expectedSeatCounts) {
		t.Errorf("Actual seat counts is: \n %+v  \n different than expected seat counts \n %+v ", approxSeatCounts, expectedSeatCounts)
	}
	// give time for two goroutines to write to the log
	// time.Sleep(1 * time.Millisecond)
	// t.Errorf("Exiting Test")
}
