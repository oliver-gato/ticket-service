// Package dirtyreadcounter performs a non-blocking read of the free seats at the venue.
// This means that the count is returned without actually accessing the venue seats counts at the request time.
// Instead, the seat count is "as of" some recent past (eg. 200ms).
// This counter is updated at a regular interval by a separate goroutine.
// Since the count returned by this module is non-blocking, it allows for handling high volume of
// read count requests without undully affecting the modification operations by the seatkeeper module.
// This module is only intended to be used by the incoming external requests.
// The seatkeeper module has exclusive access to the venue records, so it can perform actual read/count
// before it modifies the seat information.
package dirtyreadcounter

import (
	"log"
	"sync"
	"ticket-service/internal/seatkeeper"
	"ticket-service/internal/venue"
	"time"
)

//ApproxSeatCounts returns approximate number of free, held and reserved seats at the venue
type ApproxSeatCounts struct {
	Free     uint
	Held     uint
	Reserved uint
}

// ProtectedCounts is updated by a goroutine, but the counts cannot be accessed directly
// because they are part of a private field.  The protected counts needs to be used as
// a receiver of a method.
type ProtectedCounts struct {
	mutex  sync.RWMutex
	counts ApproxSeatCounts
}

// New returns a pointer to the ProtectedCounts and starts an updating goroutine.
// Many of the inputs are channels that are created by the seatkeeper module.
// seatkeeper.New() must be called first before this function can be invoked.
func New(
	updateFrequency time.Duration, // this should be a few hundered milliseconds
	quitCh seatkeeper.QuitReceiveCh,
	sendCountRequest seatkeeper.SeatCountSendCh,
	receiveCount seatkeeper.FreeSeatsReceiveCh,
) *ProtectedCounts {
	// create a pointer for a new protected counts struct
	p := &ProtectedCounts{}

	//get initial count
	actualCount := seatkeeper.RequestCountWaitForReply(
		sendCountRequest,
		receiveCount,
		quitCh,
	)
	// set the count using the method that accesses ProtectedCounts using a mutex
	p.set(actualCount)

	// start a goroutine to update the counts evey so often using similar calls as done above
	go func(
		updateFrequency time.Duration,
		p *ProtectedCounts,
		sendCountRequest seatkeeper.SeatCountSendCh,
		receiveCount seatkeeper.FreeSeatsReceiveCh,
		quitCh seatkeeper.QuitReceiveCh,
	) {
		//start a ticker channel that will expire after the input duration is counted down
		ticker := time.NewTicker(updateFrequency)

		// infinite loop
		for {
			// try reading from the quit channel, when blocked fall out of select
			// this statement is separate from other select statements because we want to quit asap
			select {
			case <-quitCh:
				log.Println("Exiting dirtyreadcounter goroutine in loop 1")
				ticker.Stop()
				return
			default:
			}
			// try reading from different channels to see what type of work needs to be done
			// this is a non-blocking attempt and if there is no work, we fall out of the select statement
			select {
			case <-quitCh:
				log.Println("Exiting dirtyreadcounter goroutine in loop 2")
				ticker.Stop()
				return
			case <-ticker.C:
				//get updated count after the ticker expired
				actualCount := seatkeeper.RequestCountWaitForReply(
					sendCountRequest,
					receiveCount,
					quitCh,
				)
				// set the count using the method that accesses ProtectedCounts using a mutex
				p.set(actualCount)
				// reset the ticker
				ticker = time.NewTicker(updateFrequency)
			default:
			}
		}
	}(
		updateFrequency,
		p,
		sendCountRequest,
		receiveCount,
		quitCh,
	)

	// return a protected count pointer
	return p
}

// Get returns the Approximate seat counts
func (p *ProtectedCounts) Get() ApproxSeatCounts {
	// obtain Read Lock
	p.mutex.RLock()
	defer p.mutex.RUnlock()

	// p holds a pointer so we have to dereference it before returning it
	return (*p).counts

}

// set is a method called by the updating goroutine
// if goroutine is started in this module, I can make set private here
func (p *ProtectedCounts) set(vc venue.SeatCounts) {
	ac := ApproxSeatCounts{
		Free:     vc.Free,
		Held:     vc.Held,
		Reserved: vc.Reserved,
	}
	// obtain Write Lock
	p.mutex.Lock()
	defer p.mutex.Unlock()

	// set a private field in the receiver of the method after obtaining Write lock on the mutex field
	p.counts = ac
}
