// Package seatkeeper starts the goroutine that has exclusive access to read and/or modify the seat reservations
// at the venue.  All requests for seat modifications need to be passed using an appropriate channel
// Biderectional channel can be passed into a function that accepts receive or send chanels in which case
// the function will be only allowed to perform a receive or send on the channel.
// Many of the channels created in this function are permanent for the duration of the service.
// This is in contrast to some temporary channels that are created in the hold and reservation modules
// These temporary channels are used for messaging between seatkeeper and short-lived, individual hold and reservation goroutines.
package seatkeeper

import (
	"log"
	"ticket-service/internal/venue"
	"time"
)

// QuitSignal is used to signal witout data
type QuitSignal struct{}

// QuitReceiveCh receives the quit signal from main
// There is one permanent channel of this type
type QuitReceiveCh <-chan QuitSignal

// QuitSendCh sends the quit signal by closing the channel (only done from main)
// There is one permanent channel of this type
type QuitSendCh chan<- QuitSignal

//SeatCountSignal is used to signal without data
type SeatCountSignal struct{}

// SeatCountRequest receives requests for number of free seats
// There is one permanent channel of this type
type seatCountReceiveCh <-chan SeatCountSignal

//SeatCountSendCh sends requests for number of free seats
// There is one permanent channel of this type
type SeatCountSendCh chan<- SeatCountSignal

// freeSeatsSendCh sends the number of free seats at the venue
// There is one permanent channel of this type
type freeSeatsSendCh chan<- venue.SeatCounts

// FreeSeatsReceiveCh receives the number of free seats at the venue
// There is one permanent channel of this type
type FreeSeatsReceiveCh <-chan venue.SeatCounts

//HoldRequestReceiveCh receives requests from holdfiller that were sent by seatkeeper
// There is one permanent channel of this type
type HoldRequestReceiveCh <-chan HoldRequestDetails

//HoldRequestSendCh sends requests from holdfiller to seatkeeper
// There is one permanent channel of this type
type HoldRequestSendCh chan<- HoldRequestDetails

// HoldReplySendCh is used to send the details of the hold request back to holdfiller
// Each new holdfiller creates a new channel of this type
type HoldReplySendCh chan<- HoldReplyDetails

// HoldReplyReceiveCh is used to receive the details of the hold request from seatkeeper
// Each new holdfiller creates a new channel of this type
type HoldReplyReceiveCh <-chan HoldReplyDetails

//HoldReplyDetails contains details about success or failure of a Hold Request
type HoldReplyDetails struct {
	HoldID venue.HoldID
	Err    error
}

// HoldCancelSendCh is used by the cancel hold goroutine to send hold cancellation requests
// There is one permanent channel of this type
type HoldCancelSendCh chan<- HoldCancelDetails

//HoldCancelReceiveCh is used by the seatkeeper routine to get hold cancellation requests
// There is one permanent channel of this type
type HoldCancelReceiveCh <-chan HoldCancelDetails

//HoldCancelDetails has information that is used to cancel hold request after timeout expiration
type HoldCancelDetails struct {
	HoldID venue.HoldID
}

//GiveUpWaitHoldSignal is used for signalling to abandon holds
type GiveUpWaitHoldSignal struct{}

// GiveUpWaitHoldRequestSendCh tells seatkeeper that holdfiller is abandoning the hold request
// Each new holdfiller creates a new channel of this type
type GiveUpWaitHoldRequestSendCh chan<- GiveUpWaitHoldSignal

// GiveUpWaitHoldRequestReceiveCh tells seatkeeper that holdfiller is abandoning the hold request
// Each new holdfiller creates a new channel of this type
type GiveUpWaitHoldRequestReceiveCh <-chan GiveUpWaitHoldSignal

//StartingHoldSignal is used to signal that hold work is starting
type StartingHoldSignal struct{}

//StartingHoldSendCh tells holdfiller that seatkeeper started to work on the hold request
// Each new holdfiller creates a new channel of this type
type StartingHoldSendCh chan<- StartingHoldSignal

//StartingHoldReceiveCh tells holdfiller that seatkeeper started to work on the hold request
// Each new holdfiller creates a new channel of this type
type StartingHoldReceiveCh <-chan StartingHoldSignal

// HoldRequestDetails contains channel for sending response back from seatkeeper to holdfiler hold information or error
type HoldRequestDetails struct {
	CustomerEmail           string
	RequestedSeats          uint
	HoldReplyDetailsCh      chan HoldReplyDetails
	GiveUpWaitHoldRequestCh chan GiveUpWaitHoldSignal
	StartingHoldCh          chan StartingHoldSignal
}

//ReservationRequestReceiveCh receives requests from reservationfiller that were sent by seatkeeper
// There is one permanent channel of this type
type ReservationRequestReceiveCh <-chan ReservationRequestDetails

//ReservationRequestSendCh sends requests from reservationfiller to seatkeeper
// There is one permanent channel of this type
type ReservationRequestSendCh chan<- ReservationRequestDetails

// ReservationReplySendCh is used to send the details of the reservation request back to reservationfiller
// Each new reservationfiller creates a new channel of this type
type ReservationReplySendCh chan<- ReservationReplyDetails

// ReservationReplyReceiveCh is used to receive the details of the reservation request from seatkeeper
// Each new reservationfiller creates a new channel of this type
type ReservationReplyReceiveCh <-chan ReservationReplyDetails

// ReservationReplyDetails contains details about success or failure of a Reservation Request
type ReservationReplyDetails struct {
	ReservationID venue.ReservationID
	Err           error
}

//GiveUpWaitReservationSignal is used for signalling to abandon waiting for reservation
type GiveUpWaitReservationSignal struct{}

// GiveUpWaitReservationRequestSendCh tells seatkeeper that reservationfiller is abandoning the reservation request
// Each new reservationfiller creates a new channel of this type
type GiveUpWaitReservationRequestSendCh chan<- GiveUpWaitReservationSignal

// GiveUpWaitReservationRequestReceiveCh tells seatkeeper that reservationfiller is abandoning the reservation request
// Each new reservationfiller creates a new channel of this type
type GiveUpWaitReservationRequestReceiveCh <-chan GiveUpWaitReservationSignal

//StartingReservationSignal is used for signalling that reservation is ready to be worked on
type StartingReservationSignal struct{}

//StartingReservationSendCh tells reservationfiller that seatkeeper started to work on the reservation request
// Each new reservationfiller creates a new channel of this type
type StartingReservationSendCh chan<- StartingReservationSignal

//StartingReservationReceiveCh tells reservationfiller that seatkeeper started to work on the reservation request
// Each new reservationfiller creates a new channel of this type
type StartingReservationReceiveCh <-chan StartingReservationSignal

//ReservationRequestDetails contains channel for sending response back from seatkeeper to reservationfiller confirmation or error
type ReservationRequestDetails struct {
	CustomerEmail                  string
	HoldID                         venue.HoldID
	ReservationReplyDetailsCh      chan ReservationReplyDetails
	GiveUpWaitReservationRequestCh chan GiveUpWaitReservationSignal
	StartingReservationCh          chan StartingReservationSignal
}

// ChannelBag is a collection of various channels that can be used to communicate with the seatkeeper goroutine
type ChannelBag struct {
	QuitReceiveCh            QuitReceiveCh             // quit all goroutines when this channel is closed
	QuitSendCh               QuitSendCh                // only a single sender can exists for this channel
	SeatCountSendCh          SeatCountSendCh           // send a value to request seat count
	FreeSeatsReceiveCh       FreeSeatsReceiveCh        // receive the seat count from the keeper
	HoldRequestSendCh        HoldRequestSendCh         // send hold requests
	ReservationRequestSendCh ReservationRequestSendCh  // send reservation request
	venue                    *venue.SeatingArrangement // this field is private because it cannot be accessed externally
}

// NewHoldChs is called by each holdfiller to create various temporary channels for
// communications between seatkeeper and one holdfiller
func NewHoldChs() (
	chan HoldReplyDetails,
	chan GiveUpWaitHoldSignal, // GiveUpWaitHoldRequest
	chan StartingHoldSignal, // StartingHold
) {
	// create one-time channels
	holdReplyDetailsCh := make(chan HoldReplyDetails, 1) // non-blocking, buffered
	giveUpWaitHoldCh := make(chan GiveUpWaitHoldSignal)  // blocking
	startingHold := make(chan StartingHoldSignal)        // blocking with data

	return holdReplyDetailsCh, giveUpWaitHoldCh, startingHold
}

// NewReservationChs is called by each reservationfiller to create various temporary channels for
// communications between seatkeeper and one reservationfiller
func NewReservationChs() (
	chan ReservationReplyDetails,
	chan GiveUpWaitReservationSignal, // GiveUpWaitReservationRequest
	chan StartingReservationSignal, // StartingReservation
) {
	// create one-time channels
	reservationReplyDetailsCh := make(chan ReservationReplyDetails, 1) // non-blocking, buffered
	giveUpWaitReservationCh := make(chan GiveUpWaitReservationSignal)  // blocking
	startingReservation := make(chan StartingReservationSignal)        // blocking with data

	return reservationReplyDetailsCh, giveUpWaitReservationCh, startingReservation
}

//New starts a goroutine and creates needed channels
//There should be ONLY 1 invocation to New in the running service
func New(
	numRows,
	seatsPerRow uint16,
	holdChSize uint8,
	reservationChSize uint8,
	cancelTimeOut time.Duration, // the hold is valid from when it was placed until this timer expires
) *ChannelBag {

	// creating a new venue that will be passed into the seatkeeper goroutine
	v := venue.NewVenue(numRows, seatsPerRow)

	// the channels are created for two way communication but when they are
	// placed in a struct or passed into a goroutine, they are restriced to sending or receiving ONLY
	//
	// all goroutines should receive on this channel and return immediately (see below for example usage)
	quit := make(chan QuitSignal) // unbuffered channel with blocking behavior

	// permenent channels that are used to communicate with seatkeeper
	freeseats := make(chan venue.SeatCounts, 1)               // buffered
	countseats := make(chan SeatCountSignal, 1)               // buffered
	holdRequests := make(chan HoldRequestDetails, holdChSize) // buffered
	cancelHold := make(chan HoldCancelDetails)                // unbuffered, does NOT get placed in the ChannelBag
	reservationRequests := make(chan ReservationRequestDetails, reservationChSize)

	cb := ChannelBag{
		QuitReceiveCh:            quit,                // all goroutines should receive from the channel
		QuitSendCh:               quit,                // only the main should close this channel
		SeatCountSendCh:          countseats,          // send seat count requests
		FreeSeatsReceiveCh:       freeseats,           // receive seat count replies
		HoldRequestSendCh:        holdRequests,        // send hold requests on this channel
		ReservationRequestSendCh: reservationRequests, // send reservation requests on this channel
		venue: v,
	}

	// this is the main seatkeeper goroutine that should have exclusive access to
	// read and/or modify seats at the venue
	go func(
		v *venue.SeatingArrangement,
		quit QuitReceiveCh,
		countSeats seatCountReceiveCh,
		countOut freeSeatsSendCh,
		holdRequests HoldRequestReceiveCh,
		reservationRequests ReservationRequestReceiveCh,
		cancelHoldSend HoldCancelSendCh, // same channel used for the next variable
		cancelHoldRec HoldCancelReceiveCh, // same channel used for the prior variables
		cancelTimeOut time.Duration, // holds are reverted to free seats if not reserved before this timer expires
	) {
		// infinite loop
		for {
			//
			// try reading from the quit channel, when blocked fall out of select
			// this statement is separate from other select statements because we want to quit asap
			//
			select {
			case <-quit:
				log.Println("Exiting seatkeeper goroutine in select 1")
				return
			default:
			}
			//
			// try reading from the hold request channel, hold cancel channel as well as
			// reservation request channel
			// when blocked (i.e. no hold requests) then fall out of select
			//
			select {
			case <-quit:
				log.Println("Exiting seatkeeper goroutine in select 2")
				return
			// check if there are any cancel hold requests
			case holdCancelDetails := <-cancelHoldRec:
				err := v.ReleaseHeldSeats(holdCancelDetails.HoldID)
				// the only place that we can communicate the cancel hold errors is the log
				if err != nil {
					log.Println("Cancel hold request could not be completed")
				}
			// check if there are any hold requests
			case holdRequest := <-holdRequests:
				performHoldWork(quit, holdRequest, v, cancelHoldSend, cancelTimeOut, holdRequest.CustomerEmail)
			// check if there are any reservation requests
			case reservationRequest := <-reservationRequests:
				performReservationWork(quit, reservationRequest, v, reservationRequest.CustomerEmail)
			default:
			}
			//
			// try reading from different channels to see what type of work needs to be done
			// this is a non-blocking attempt and if there is no work, we fall out of the select statement
			//
			select {
			case <-quit:
				log.Println("Exiting seatkeeper goroutine in select 3")
				return
			case <-countSeats:
				seatCounts := v.NumFreeSeats()
				// the output channel is non-blocking but just in case
				// the other side did not receive the message we are going with the try-send logic
				// if I hit the default case then something is wrong (e.g. the other goroutine got stuck
				// and cannot process messages sent on this channel)
				select {
				case countOut <- seatCounts:
				default:
					log.Println("seatkeeper: could not write to dirtyreadcounter channel")
				}
			default:
			}

		}
	}(
		v,
		quit,
		countseats,
		freeseats,
		holdRequests,
		reservationRequests,
		cancelHold,
		cancelHold,
		cancelTimeOut,
	)

	// return pointer to ChannelBag
	return &cb
}

// performReservationWork is invoked when a reservation request is received; it is executing a blocking select
func performReservationWork(
	quit QuitReceiveCh,
	reservationRequest ReservationRequestDetails,
	v *venue.SeatingArrangement,
	customerEmail string,
) {
	// acknowlege that reservation was received and begin the reservation work: this is a blocking select!
	// there are two choices: reservationfiller received message that seatkeeper is starting the reservation work
	// or reservationfiller sent a message that it gave up on the reservationfiller request
	select {
	case <-quit:
		log.Println("Exiting seatkeeper goroutine in reservation request select")
		return
	case reservationRequest.StartingReservationCh <- struct{}{}:
		log.Println("seatkeeper: Reservation request started")
		rID, err := v.ReserveSeats(reservationRequest.HoldID, customerEmail)
		reservationDetails := ReservationReplyDetails{
			ReservationID: rID,
			Err:           err,
		}
		reservationRequest.ReservationReplyDetailsCh <- reservationDetails
	case <-reservationRequest.GiveUpWaitReservationRequestCh:
		//when reservationfiller function returned and gave up on the reservation request then simply fall out
		log.Println("seatkeeper: Detected that reservationfiller gave up waiting for reservation request")
		v.IncrementGiveUpWaitForReservationCounter()
	}
}

//performHoldWork is invoked when a hold request is received; it is executing a blocking select
func performHoldWork(
	quit QuitReceiveCh,
	holdRequest HoldRequestDetails,
	v *venue.SeatingArrangement,
	cancelHoldSend HoldCancelSendCh,
	cancelTimeOut time.Duration,
	customerEmail string,
) {
	// acknowlege that hold was received and begin the hold work: this is a blocking select!
	// there are two choices: holdfiller received message that seatkeeper is starting the hold work
	// or holdfiller sent a message that it gave up on the hold request
	select {
	case <-quit:
		log.Println("Exiting seatkeeper goroutine in hold request select")
		return
	case holdRequest.StartingHoldCh <- struct{}{}:
		log.Println("seatkeeper: Hold request started")
		hID, err := v.HoldSeats(holdRequest.RequestedSeats, customerEmail)
		holdDetails := HoldReplyDetails{
			HoldID: hID,
			Err:    err,
		}
		holdRequest.HoldReplyDetailsCh <- holdDetails
		//
		// if there were no errors when making hold request,
		// start a goroutine with a timer to cancel hold request if not reserved within given duration
		//
		if holdDetails.Err == nil {
			holdCancelDetails := HoldCancelDetails{HoldID: holdDetails.HoldID}
			startHoldCancelTimer(quit, cancelHoldSend, cancelTimeOut, holdCancelDetails)
		}
	case <-holdRequest.GiveUpWaitHoldRequestCh:
		//when holdfiller function returned and gave up on the hold request then simply fall out
		log.Println("seatkeeper: Detected that holdfiller gave up waiting for hold request")
		v.IncrementGiveUpWaitForHoldCounter()
	}
}

//startHoldCancelTimer kicks off a goroutine that will report back to seatkeeper main routine
// after pre-determined amount of time to indicate that hold time has expired
// the main seatkeeper goroutine can only cancel the hold if it has not been converted to a reservation
func startHoldCancelTimer(
	quit QuitReceiveCh,
	cancelHoldSend HoldCancelSendCh,
	cancelTimeOut time.Duration,
	holdCancelDetails HoldCancelDetails,
) {
	go func(
		quit QuitReceiveCh,
		cancelHoldSend HoldCancelSendCh,
		cancelTimeOut time.Duration,
		holdCancelDetails HoldCancelDetails,
	) {
		cancelTimer := time.NewTimer(cancelTimeOut)

		// blocking select for two unbuffered channel
		// the first signal that can be received is the winner
		select {
		case <-quit:
			log.Println("Exiting cancel hold goroutine")
			return
		case <-cancelTimer.C:
			// this is a blocking send as well
			cancelHoldSend <- holdCancelDetails
		}
	}(
		quit,
		cancelHoldSend,
		cancelTimeOut,
		holdCancelDetails,
	)
}

// RequestCountWaitForReply uses channels created by seatkeeper to communicate with it
// by requesting an actual seat count and waiting for reply
// this function is used by tests and dirtyreadcounter module
func RequestCountWaitForReply(
	request SeatCountSendCh,
	reply FreeSeatsReceiveCh,
	quit QuitReceiveCh,
) venue.SeatCounts {
	// request a seat count by sending value on the channel
	request <- struct{}{}
	// block until the seats are sent back or quit channel is closed
	select {
	case <-quit:
		return venue.SeatCounts{}
	case seatCounts := <-reply:
		// log.Println("Returning seat counts in RequestCountWaitForReply")
		return seatCounts
	}
}

//ShowInternalStats returns the internal stats and can be used in tests
//It is only safe to access after a seatkeeper goroutine exits which happens when quit channel is closed
func (cb *ChannelBag) ShowInternalStats() venue.InternalStats {
	return (cb.venue).ShowInternalStats()
}
