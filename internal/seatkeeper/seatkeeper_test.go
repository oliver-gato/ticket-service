package seatkeeper_test

import (
	"reflect"
	"testing"
	"ticket-service/internal/seatkeeper"
	"ticket-service/internal/venue"
	"time"
)

func TestShouldInteractWithSeatkeeperRoutine1(t *testing.T) {
	// start a seatkeeper goroutine
	cb := seatkeeper.New(3, 5, 1, 1, 200*time.Millisecond)

	// wait for response from the seatkeeper
	// the quit channel is probably not needed but it is a precaution since we are starting an infinite loop
	actualSeatCounts := seatkeeper.RequestCountWaitForReply(cb.SeatCountSendCh, cb.FreeSeatsReceiveCh, cb.QuitReceiveCh)

	//all routines must exit, this can only be called from the same location that called seatkeeper.New()
	close(cb.QuitSendCh)

	expectedSeatCounts := venue.SeatCounts{Free: 15, Held: 0, Reserved: 0}
	if !reflect.DeepEqual(actualSeatCounts, expectedSeatCounts) {
		t.Errorf("Actual seat counts is: \n %+v  \n different than expected seat counts \n %+v ", actualSeatCounts, expectedSeatCounts)
	}
	// give seatkeeper time to write to the log
	// time.Sleep(1 * time.Millisecond)
	// t.Errorf("Exiting Test")
	actualStat := cb.ShowInternalStats()
	expectedStat := venue.InternalStats{
		SeatCountRequests:   1,
		HoldSeatRequests:    0,
		ReserveSeatRequests: 0,
		// CancelHoldSeatRequests: 0,
	}
	if !reflect.DeepEqual(actualStat, expectedStat) {
		t.Errorf("Actual stats is: \n %+v  \n different than expected stats \n %+v ", actualStat, expectedStat)
	}
}
