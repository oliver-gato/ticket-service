// Package errorcode defines error codes that can be inspected in unit tests
// It implements the errorer interface from the standard library
package errorcode

// after installing stringer
//go get golang.org/x/tools/cmd/stringer
// run this command Linux: go generate internal/errorcode/errorcode.go

//go:generate stringer -type=code
type code int

// enumeration for error codes
const (
	HoldChannelFull code = iota + 1
	HoldTimerExpired
	HoldMustRequestMoreThanZeroSeats
	HoldNotEnoughFreeSeats
	HoldIteration
	RowOffsetOutOfBounds
	SeatffsetOutOfBounds
	SeatHeldInHoldRequest
	SeatReservedInHoldRequest
	ReleaseSeatNotHeld
	ReleaseSeatWrongHoldID
	ReleaseSeatIsReserved
	ReserveSeatNotHeld
	ReserveSeatWrongID
	ReserveSeatNotValid
	ReservationChannelFull
	ReservationTimerExpired
)

type details struct {
	err code
	msg string
}

//Error implements the Errorer interface from the standard library
func (d details) Error() string {
	return d.msg
}

// IotaError shows the string representation of the iota error enumeration
func IotaError(e error) string {
	switch e.(type) {
	case details:
		//perform type assertion
		return e.(details).err.String()
	}
	return e.Error()
}

//New creates new custom error
func New(c code, s string) error {
	return details{err: c, msg: s}
}
