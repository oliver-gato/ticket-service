package venue_test

import (
	"reflect"
	"testing"
	"ticket-service/internal/testutil"
	"ticket-service/internal/venue"
)

// hold off writing unit tests for error conditions in case I want to start using another package for
// comparing the expected error message which would change the testing logic.

func commandToProduceGolden(testName string) string {
	cmd := "Run this command to update the golden file: \n" + "go test -v ./internal/venue/venue_test.go -test.run " + testName + " -update=true"
	return cmd
}

func TestShouldCheckSeatingChart1(t *testing.T) {

	v := venue.NewVenue(3, 5)
	// 3 rows with 5 seats per row

	actualString := v.PrintChart()
	testutil.HelperForTest(t.Name(), t, commandToProduceGolden(t.Name()), []byte(actualString), "")
	actualStat := v.ShowInternalStats()
	expectedStat := venue.InternalStats{}
	if !reflect.DeepEqual(actualStat, expectedStat) {
		t.Errorf("Actual stats is: \n %+v  \n different than expected stats \n %+v ", actualStat, expectedStat)
	}

}

func TestShouldCheckSeatingChart2(t *testing.T) {

	v := venue.NewVenue(3, 5)
	_, err := v.HoldSeats(3, "email")
	if err != nil {
		t.Errorf(err.Error())
	}

	// 3 rows with 5 seats per row
	actualString := v.PrintChart()
	testutil.HelperForTest(t.Name(), t, commandToProduceGolden(t.Name()), []byte(actualString), "")

	actualSeatCounts := v.NumFreeSeats()
	expectedSeatCounts := venue.SeatCounts{Free: 12, Held: 3, Reserved: 0}
	if !reflect.DeepEqual(actualSeatCounts, expectedSeatCounts) {
		t.Errorf("Actual seat counts is: \n %+v  \n different than expected seat counts \n %+v ", actualSeatCounts, expectedSeatCounts)
	}
	actualStat := v.ShowInternalStats()
	expectedStat := venue.InternalStats{
		SeatCountRequests:   1,
		HoldSeatRequests:    1,
		ReserveSeatRequests: 0,
		// CancelHoldSeatRequests: 0,
	}
	if !reflect.DeepEqual(actualStat, expectedStat) {
		t.Errorf("Actual stats is: \n %+v  \n different than expected stats \n %+v ", actualStat, expectedStat)
	}

}

func TestShouldCheckSeatingChart3(t *testing.T) {

	v := venue.NewVenue(3, 5)

	hID, err := v.HoldSeats(3, "email")
	if err != nil {
		t.Errorf(err.Error())
	}
	_, err = v.HoldSeats(4, "email")
	if err := v.ReleaseHeldSeats(hID); err != nil {
		t.Errorf(err.Error())
	}

	actualString := v.PrintChart()
	testutil.HelperForTest(t.Name(), t, commandToProduceGolden(t.Name()), []byte(actualString), "")

	actualSeatCounts := v.NumFreeSeats()
	expectedSeatCounts := venue.SeatCounts{Free: 11, Held: 4, Reserved: 0}
	if !reflect.DeepEqual(actualSeatCounts, expectedSeatCounts) {
		t.Errorf("Actual seat counts is: \n %+v  \n different than expected seat counts \n %+v ", actualSeatCounts, expectedSeatCounts)
	}

	actualStat := v.ShowInternalStats()
	expectedStat := venue.InternalStats{
		SeatCountRequests:   1,
		HoldSeatRequests:    2,
		ReserveSeatRequests: 0,
		// CancelHoldSeatRequests: 1,
	}
	if !reflect.DeepEqual(actualStat, expectedStat) {
		t.Errorf("Actual stats is: \n %+v  \n different than expected stats \n %+v ", actualStat, expectedStat)
	}
}

func TestShouldCheckSeatingChart4(t *testing.T) {

	v := venue.NewVenue(3, 5)

	hID, err := v.HoldSeats(3, "email")
	if err != nil {
		t.Errorf(err.Error())
	}
	hID2, err := v.HoldSeats(4, "email")
	if err := v.ReleaseHeldSeats(hID); err != nil {
		t.Errorf(err.Error())
	}
	_, err = v.ReserveSeats(hID2, "email")
	if err != nil {
		t.Errorf(err.Error())
	}
	_, err = v.HoldSeats(2, "email")
	if err != nil {
		t.Errorf(err.Error())
	}

	actualString := v.PrintChart()
	testutil.HelperForTest(t.Name(), t, commandToProduceGolden(t.Name()), []byte(actualString), "")

	actualSeatCounts := v.NumFreeSeats()
	expectedSeatCounts := venue.SeatCounts{Free: 9, Held: 2, Reserved: 4}
	if !reflect.DeepEqual(actualSeatCounts, expectedSeatCounts) {
		t.Errorf("Actual seat counts is: \n %+v  \n different than expected seat counts \n %+v ", actualSeatCounts, expectedSeatCounts)
	}

	actualStat := v.ShowInternalStats()
	expectedStat := venue.InternalStats{
		SeatCountRequests:   1,
		HoldSeatRequests:    3,
		ReserveSeatRequests: 1,
		// CancelHoldSeatRequests: 1,
	}
	if !reflect.DeepEqual(actualStat, expectedStat) {
		t.Errorf("Actual stats is: \n %+v  \n different than expected stats \n %+v ", actualStat, expectedStat)
	}
}
