// Package venue defines the seating arrangement and exposes methods to hold and reserve seats.
// Since a new venue SeatingArrangement is created inside of the seatkeeper goroutine, venue's public methods
// are only accessible to the seatkeeper routine
package venue

import (
	"fmt"
	"strings"
	"ticket-service/internal/errorcode"
)

const freeSeat = `S`
const heldSeat = `H`
const reservedSeat = `R`

// HoldID is the type for holds
type HoldID int

// ReservationID is the type for reservations
type ReservationID int

type held struct {
	isValid bool
	HoldID
}

type reserved struct {
	isValid bool
	ReservationID
}
type seat struct {
	held
	reserved
}
type sequenceID struct {
	HoldID
	ReservationID
}

// row is a slice/list of seats
type row []seat

// matrix is a slice/list of rows
type matrix []row

// InternalStats keeps track of internal seatkeeper counters
// For now, it is ONLY safe to access this struct after the seatkeeper routine is sent a quit request
type InternalStats struct {
	SeatCountRequests   uint
	HoldSeatRequests    uint
	ReserveSeatRequests uint
	// CancelHoldSeatRequests           uint  // I currently do not have a good way to test this
	GiveUpWaitForHoldRequests        uint
	GiveUpWaitForReservationRequests uint
}

//IncrementGiveUpWaitForHoldCounter adds 1 to the internal counter
func (s *SeatingArrangement) IncrementGiveUpWaitForHoldCounter() {
	(*s).internalStats.GiveUpWaitForHoldRequests++
}

//IncrementGiveUpWaitForReservationCounter adds 1 to the internal counter
func (s *SeatingArrangement) IncrementGiveUpWaitForReservationCounter() {
	(*s).internalStats.GiveUpWaitForReservationRequests++
}

//SeatingArrangement struct holds the master copy of the seats at the venue
// all fields are private which forces other modules to use public methods
// with this struct as a receiver
type SeatingArrangement struct {
	matrix
	internalStats InternalStats
	sequenceID
	seatCounts SeatCounts
	// TO DO: add a special data structure that will be an index for the seating matrix
}

// SeatCounts returns the seat counts for the venue
type SeatCounts struct {
	Free     uint
	Held     uint
	Reserved uint
}

// NewVenue allocates memory for an empty venue given the number of rows and seats for each row
// it returns a pointer to the struct, the two inputs must be in the range of 0 through 65535 (uint16)
func NewVenue(numRows, seatsPerRow uint16) *SeatingArrangement {
	oneMatrix := make(matrix, numRows)
	for i := uint16(0); i < numRows; i++ {
		oneRow := make(row, seatsPerRow)
		for k := uint16(0); k < seatsPerRow; k++ {
			oneRow[k] = seat{}
		}
		oneMatrix[i] = oneRow
	}
	s := &SeatingArrangement{matrix: oneMatrix}
	updateSeatCounts(s)
	return s
}

func seatCode(t seat) string {
	if !t.held.isValid && !t.reserved.isValid {
		// the seat if free
		return freeSeat
	}
	if !t.reserved.isValid {
		// the seat is held
		return heldSeat
	}
	// otherwise the seat is reserved
	return reservedSeat

}

func getNextHoldID(s *SeatingArrangement) HoldID {
	s.sequenceID.HoldID++
	return s.sequenceID.HoldID
}

func getNextReservationID(s *SeatingArrangement) ReservationID {
	s.sequenceID.ReservationID++
	return s.sequenceID.ReservationID
}

//HoldSeats places a hold on the seats.
// the search algorithm will change in the future to lookup seats using an index structure
func (s *SeatingArrangement) HoldSeats(requestedSeats uint, customerEmail string) (
	HoldID,
	error,
) {
	s.internalStats.HoldSeatRequests++
	if requestedSeats == 0 {
		return 0, errorcode.New(errorcode.HoldMustRequestMoreThanZeroSeats, errorcode.HoldMustRequestMoreThanZeroSeats.String())
	}
	hID := getNextHoldID(s)
	freeSeats := s.seatCounts.Free
	if freeSeats < requestedSeats {
		return 0, errorcode.New(errorcode.HoldNotEnoughFreeSeats, fmt.Sprintf("Requesting to hold %d seats but only %d are free", requestedSeats, freeSeats))
	}
	err := iterateToHoldSeats(s, hID, requestedSeats)
	if err != nil {
		updateSeatCounts(s)
		return 0, err
	}
	updateSeatCounts(s)
	return hID, nil
}

func iterateToHoldSeats(s *SeatingArrangement, hID HoldID, requestedSeats uint) error {
	var heldSeats uint
	for rowOffset := range s.matrix {
		for seatOffset := range s.matrix[rowOffset] {
			code := seatCode(s.matrix[rowOffset][seatOffset])
			if code == freeSeat {
				heldSeats++
				err := holdSeat(s, uint16(rowOffset), uint16(seatOffset), hID)
				if err != nil {
					return err
				}
				if heldSeats == requestedSeats {
					return nil
				}
			}
		}
	}
	return errorcode.New(errorcode.HoldIteration, fmt.Sprintf("Reached end of iteration without finding %d seats for hold id %d", requestedSeats, hID))
}

// ReleaseHeldSeats removes hold from the seats
func (s *SeatingArrangement) ReleaseHeldSeats(hID HoldID) error {
	// s.internalStats.CancelHoldSeatRequests++
	err := iterateToReleaseSeats(s, hID)
	if err != nil {
		updateSeatCounts(s)
		return err
	}
	updateSeatCounts(s)
	return nil
}
func iterateToReleaseSeats(s *SeatingArrangement, hID HoldID) error {
	for rowOffset := range s.matrix {
		for seatOffset := range s.matrix[rowOffset] {
			code := seatCode(s.matrix[rowOffset][seatOffset])
			if code == heldSeat && s.matrix[rowOffset][seatOffset].held.HoldID == hID {
				err := releaseHold(s, uint16(rowOffset), uint16(seatOffset), hID)
				if err != nil {
					return err
				}
			}
		}
	}
	return nil
}

// ReserveSeats confirms held seats
func (s *SeatingArrangement) ReserveSeats(hID HoldID, customerEmail string) (ReservationID, error) {
	s.internalStats.ReserveSeatRequests++
	rID := getNextReservationID(s)
	err := iterateToReserveSeats(s, hID, rID)
	if err != nil {
		updateSeatCounts(s)
		return 0, err
	}
	updateSeatCounts(s)
	return rID, nil
}

func iterateToReserveSeats(s *SeatingArrangement, hID HoldID, rID ReservationID) error {
	for rowOffset := range s.matrix {
		for seatOffset := range s.matrix[rowOffset] {
			code := seatCode(s.matrix[rowOffset][seatOffset])
			if code == heldSeat && s.matrix[rowOffset][seatOffset].held.HoldID == hID {
				err := reserveHeldSeat(s, uint16(rowOffset), uint16(seatOffset), hID, rID)
				if err != nil {
					return err
				}
			}
		}
	}
	return nil
}

func checkMatrixBounds(m matrix, rowOffset, seatOffset uint16) error {
	if len(m) <= int(rowOffset) {
		return errorcode.New(errorcode.RowOffsetOutOfBounds, fmt.Sprintf("Row offset %d out of bounds %d", rowOffset, len(m)))
	}
	if len(m[rowOffset]) <= int(seatOffset) {
		return errorcode.New(errorcode.SeatffsetOutOfBounds, fmt.Sprintf("Seat offset %d out of bounds %d", seatOffset, len(m[rowOffset])))
	}
	return nil
}

// holdSeat places a hold on the seat
func holdSeat(s *SeatingArrangement, rowOffset, seatOffset uint16, hID HoldID) error {
	err := checkMatrixBounds(s.matrix, rowOffset, seatOffset)
	if err != nil {
		return err
	}

	if s.matrix[rowOffset][seatOffset].held.isValid {
		return errorcode.New(errorcode.SeatHeldInHoldRequest, fmt.Sprintf("Seat at row offset %d and seat offset %d is currently held", rowOffset, seatOffset))
	}
	if s.matrix[rowOffset][seatOffset].reserved.isValid {
		return errorcode.New(errorcode.SeatReservedInHoldRequest, fmt.Sprintf("Seat at row offset %d and seat offset %d is already reserved", rowOffset, seatOffset))
	}

	s.matrix[rowOffset][seatOffset].held.isValid = true
	s.matrix[rowOffset][seatOffset].held.HoldID = hID
	return nil

}

// releaseHold removes a hold from the seat making it available again
func releaseHold(s *SeatingArrangement, rowOffset, seatOffset uint16, hID HoldID) error {
	err := checkMatrixBounds(s.matrix, rowOffset, seatOffset)
	if err != nil {
		return err
	}

	if !s.matrix[rowOffset][seatOffset].held.isValid {
		return errorcode.New(errorcode.ReleaseSeatNotHeld, fmt.Sprintf("Seat at row offset %d and seat offset %d does not have a hold", rowOffset, seatOffset))
	}
	if s.matrix[rowOffset][seatOffset].held.HoldID != hID {
		return errorcode.New(errorcode.ReleaseSeatWrongHoldID, fmt.Sprintf("Release: requested HoldID %d does not match with seat HoldID %d", hID, s.matrix[rowOffset][seatOffset].held.HoldID))
	}
	if s.matrix[rowOffset][seatOffset].reserved.isValid {
		return errorcode.New(errorcode.ReleaseSeatIsReserved, fmt.Sprintf("Release: seat at row offset %d and seat offset %d is already reserved", rowOffset, seatOffset))
	}
	s.matrix[rowOffset][seatOffset].held.isValid = false
	s.matrix[rowOffset][seatOffset].held.HoldID = 0

	return nil
}

//reserveHeldSeat completes the reservation process
func reserveHeldSeat(s *SeatingArrangement, rowOffset, seatOffset uint16, hID HoldID, rID ReservationID) error {
	err := checkMatrixBounds(s.matrix, rowOffset, seatOffset)
	if err != nil {
		return err
	}
	// repeat of errors that were in ReleaseHold
	if !s.matrix[rowOffset][seatOffset].held.isValid {
		return errorcode.New(errorcode.ReserveSeatNotHeld, fmt.Sprintf("Seat at row offset %d and seat offset %d does not have a hold", rowOffset, seatOffset))
	}
	if s.matrix[rowOffset][seatOffset].held.HoldID != hID {
		return errorcode.New(errorcode.ReserveSeatWrongID, fmt.Sprintf("Reserve: requested HoldID %d does not match with seat HoldID %d", hID, s.matrix[rowOffset][seatOffset].held.HoldID))
	}
	if s.matrix[rowOffset][seatOffset].reserved.isValid {
		return errorcode.New(errorcode.ReserveSeatNotValid, fmt.Sprintf("Reserve: seat at row offset %d and seat offset %d is already reserved", rowOffset, seatOffset))
	}

	s.matrix[rowOffset][seatOffset].reserved.isValid = true
	s.matrix[rowOffset][seatOffset].reserved.ReservationID = rID

	return nil
}

//PrintChart displays the availability chart for the seating arrangement
// it will be mainly used for debugging
func (s *SeatingArrangement) PrintChart() string {
	if s == nil || len(s.matrix) == 0 {
		return "Venue not defined"
	}
	var sb strings.Builder
	for rowOffset := range s.matrix {
		for seatOffset := range s.matrix[rowOffset] {
			sb.WriteString(seatCode(s.matrix[rowOffset][seatOffset]))
		}
		sb.WriteString("\n")
	}
	sb.WriteString("\n")
	return sb.String()
}

func updateSeatCounts(s *SeatingArrangement) {
	var f, h, r uint // initialized to 0

	for rowOffset := range s.matrix {
		for seatOffset := range s.matrix[rowOffset] {
			switch code := seatCode(s.matrix[rowOffset][seatOffset]); code {
			case reservedSeat:
				r++
			case heldSeat:
				h++
			case freeSeat:
				f++
			}
		}
	}

	s.seatCounts = SeatCounts{Free: f, Held: h, Reserved: r}

}

// NumFreeSeats returns the number of seats at the venue
func (s *SeatingArrangement) NumFreeSeats() SeatCounts {
	s.internalStats.SeatCountRequests++
	return s.seatCounts
}

//ShowInternalStats returns the internal stats and can be used in tests
func (s *SeatingArrangement) ShowInternalStats() InternalStats {
	return s.internalStats
}
