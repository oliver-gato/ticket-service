package reservationfiller_test

import (
	"reflect"
	"testing"
	"ticket-service/internal/errorcode"
	"ticket-service/internal/holdfiller"
	"ticket-service/internal/reservationfiller"
	"ticket-service/internal/seatkeeper"
	"ticket-service/internal/venue"
	"time"
)

// in this test, we are making 3 holds of 5 seats each but only confirm/reserve 2 of these holds
func helperReservationTest(
	t *testing.T,
	giveupDuration time.Duration,
	holdChSize uint8,
	cancelHoldTimeOut time.Duration,
	reservationChSize uint8,
) {
	rows := uint16(3)
	seatsPerRow := uint16(5)
	// start a seatkeeper goroutine
	cb := seatkeeper.New(rows, seatsPerRow, holdChSize, reservationChSize, cancelHoldTimeOut)

	holdRequests := 3
	holdReplyCh := make(chan seatkeeper.HoldReplyDetails, holdRequests)
	// start hold request routines with the same number of seats they try to hold
	for e := 0; e < holdRequests; e++ {
		go func(requestedSeats uint, holdRepCh chan seatkeeper.HoldReplyDetails) {
			hID, err := holdfiller.New(
				requestedSeats,
				"email@aol.com",
				giveupDuration,
				cb.QuitReceiveCh,
				cb.HoldRequestSendCh,
			)
			// send back the responses to the test
			holdReplyCh <- seatkeeper.HoldReplyDetails{HoldID: hID, Err: err}

		}(uint(seatsPerRow), holdReplyCh)
	}
	var expectedHoldSeatRequests uint
	var expectedGiveUpWaitForHoldRequests uint
	var expectedReservedSeats uint
	reservationReplyCh := make(chan seatkeeper.ReservationReplyDetails, holdRequests)
	// iterate over the channel to receive all messages from holdfiller goroutines
	for e := 0; e < holdRequests; e++ {
		replyDetails := <-holdReplyCh
		err := replyDetails.Err
		if err == nil {
			expectedHoldSeatRequests++
		}
		if err != nil {
			switch r := errorcode.IotaError(err); r {
			case errorcode.HoldMustRequestMoreThanZeroSeats.String():
				expectedHoldSeatRequests++
			case errorcode.HoldNotEnoughFreeSeats.String():
				expectedHoldSeatRequests++
			case errorcode.HoldTimerExpired.String():
				expectedGiveUpWaitForHoldRequests++
			}
		}
		// for even requests, make a reservation
		if e/2 == 0 {
			go func(requestedSeats uint, holdRepCh chan seatkeeper.HoldReplyDetails) {
				rID, err := reservationfiller.New(
					replyDetails.HoldID,
					"email@aol.com",
					giveupDuration,
					cb.QuitReceiveCh,
					cb.ReservationRequestSendCh,
				)
				// send back the responses to the test
				reservationReplyCh <- seatkeeper.ReservationReplyDetails{ReservationID: rID, Err: err}

			}(uint(seatsPerRow), holdReplyCh)
		}
	}
	// iterate over the channel to receive all messages from reservationfiller goroutines
	for e := 0; e < holdRequests; e++ {
		if e/2 == 0 {
			reserveDetails := <-reservationReplyCh
			err := reserveDetails.Err
			// when there was no error in the reservation request, augment the expected counter
			if err == nil {
				expectedReservedSeats++
			}
		}
	}

	// when checking seating arrangement, I could sleep here to cancel the hold on seats without reservation
	// time.Sleep(cancelHoldTimeOut)
	//all routines must exit, this can only be called from the same location that called seatkeeper.New()
	close(cb.QuitSendCh)

	actualStat := cb.ShowInternalStats()
	expectedStat := venue.InternalStats{
		SeatCountRequests:   0,
		HoldSeatRequests:    expectedHoldSeatRequests,
		ReserveSeatRequests: expectedReservedSeats,
		//CancelHoldSeatRequests:    0,
		GiveUpWaitForHoldRequests: expectedGiveUpWaitForHoldRequests,
	}
	if !reflect.DeepEqual(actualStat, expectedStat) {
		t.Errorf("Actual stats is: \n %+v  \n different than expected stats \n %+v ", actualStat, expectedStat)
	}
}

func TestShouldInteractWithReservationFillers1(t *testing.T) {
	helperReservationTest(t, 10*time.Millisecond, 5, 200*time.Millisecond, 10)
}
