// Package reservationfiller sends and receives messages from the seatkeeper goroutine to reserve held seats.
// Incoming webrequests are already running as goroutines so there is no need to start a goroutine in this module.
// Most of the logic is simillar to the holdfiller module with one exception: there is no canceling reservation requests in seatkeeper module,
// but there still exists a time out on the amount of time to wait for the seatkeeper to start working on the reservation request
// The size of the reservation request channel should be chosen carefully but it should be bigger than hold request channel
// The time out for giving up on the reservation request should be bigger than a similar time out for holds
package reservationfiller

import (
	"context"
	"log"
	"ticket-service/internal/errorcode"
	"ticket-service/internal/seatkeeper"
	"ticket-service/internal/venue"
	"time"
)

//New creates a new reservation request
func New(
	holdID venue.HoldID,
	customerEmail string,
	waitForReservation time.Duration, // e.g. 50 * time.Millisecond
	quit seatkeeper.QuitReceiveCh,
	reservationReqCh seatkeeper.ReservationRequestSendCh,
) (
	venue.ReservationID,
	error,
) {

	reservationReplyDetailsCh, giveUpWaitReservationCh, startingReservation := seatkeeper.NewReservationChs()

	reservationReqDetail := seatkeeper.ReservationRequestDetails{
		CustomerEmail: customerEmail,
		HoldID:        holdID,
		ReservationReplyDetailsCh:      reservationReplyDetailsCh,
		GiveUpWaitReservationRequestCh: giveUpWaitReservationCh,
		StartingReservationCh:          startingReservation,
	}

	// try to send the reservation request, if the channel is full then exit this function
	select {
	// send details of the hold request
	case reservationReqCh <- reservationReqDetail:
	case <-quit:
		log.Println("reservationfiller: Exiting reservationfiller in select 1")
		return 0, nil
	default:
		log.Println("reservationfiller: reservation request channel full")
		return 0, errorcode.New(errorcode.ReservationChannelFull, errorcode.ReservationChannelFull.String())
	}

	// start a context timer channel that expires after a given duration
	// this is how long we wait for reply from the seatkeeper
	ctx, cancel := context.WithTimeout(context.Background(), waitForReservation)
	defer cancel()

	//blocking select until I can receive on one of the unbuffered channels
	select {
	case <-quit:
		log.Println("reservationfiller: Exiting reservationfiller in select 2")
		return 0, nil
	case <-startingReservation:
		log.Println("reservationfiller: Reservationfiller notified that seatkeeper is starting to work on a reservation")
		reservationDetails := <-reservationReplyDetailsCh
		return reservationDetails.ReservationID, reservationDetails.Err
	case <-ctx.Done():
		log.Println("reservationfiller: reservationfiller timer expired")
		// send empty struct on the giveUpWaitReservationCh channel (unbuffered),
		// but we want to exit immediately so the send is done in a goroutine
		// if reservationfiller.New() function exits after sending the message on non-blocking channel,
		//the channel could be garbage collected before seatkeeper reads the message
		go func(
			quit seatkeeper.QuitReceiveCh,
			giveUpWaitReservationCh seatkeeper.GiveUpWaitReservationRequestSendCh,
		) {
			select {
			case <-quit:
				log.Println("reservationfiller: Exiting reservationfiller goroutine in select 3 ")
				return
			case giveUpWaitReservationCh <- struct{}{}:
				log.Println("reservationfiller: Sending message on giveUpWaitReservationCh")
				return
			}
		}(quit, giveUpWaitReservationCh)
		return 0, errorcode.New(errorcode.ReservationTimerExpired, errorcode.ReservationTimerExpired.String())
	}
}
