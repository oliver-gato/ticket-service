package holdfiller_test

import (
	"reflect"
	"testing"
	"ticket-service/internal/dirtyreadcounter"
	"ticket-service/internal/errorcode"
	"ticket-service/internal/holdfiller"
	"ticket-service/internal/seatkeeper"
	"ticket-service/internal/venue"
	"time"
)

//
// These unit tests simulate multiple concurrent requests to hold seats executed by independent routines
// Since the results vary between executions, we must
// calculate the expected responses based on two assumptions:
//
// 1. Count specific error codes returned by seatkeeper to come up with expected stats
// given various channel sizes and wait durations of hold requests
// OR
// 2. Get expected seat counts from dirtyreadcounter after we waited long enough for all holds to be cancelled
//
func helperHoldTest(
	t *testing.T,
	giveupDuration time.Duration,
	holdChSize uint8,
	cancelHoldTimeOut time.Duration,
	waitBeforeQuit bool, // when set to true, it gives seatkeeper time to cancel all holds
	readFrequency time.Duration, // dirtyread will refresh seat counts at this frequency (it only applies when waitBeforeQuit = true)
) {
	rows := uint16(3)
	seatsPerRow := uint16(5)
	// start a seatkeeper goroutine
	cb := seatkeeper.New(rows, seatsPerRow, holdChSize, 1, cancelHoldTimeOut)

	holdRequests := 10
	holdReplyCh := make(chan seatkeeper.HoldReplyDetails, holdRequests)
	// start bunch of hold request routines
	for e := 0; e < holdRequests; e++ {
		go func(requestedSeats uint, holdRepCh chan seatkeeper.HoldReplyDetails) {
			hID, err := holdfiller.New(
				requestedSeats,
				"email@aol.com",
				giveupDuration,
				cb.QuitReceiveCh,
				cb.HoldRequestSendCh,
			)
			// send back the responses to the test
			holdReplyCh <- seatkeeper.HoldReplyDetails{HoldID: hID, Err: err}

		}(uint(e), holdReplyCh)
	}
	var expectedHoldSeatRequests uint
	var expectedGiveUpWaitForHoldRequests uint
	// iterate over the channel to receive all messages from the goroutines
	for e := 0; e < holdRequests; e++ {
		replyDetails := <-holdReplyCh
		err := replyDetails.Err
		if err == nil {
			expectedHoldSeatRequests++
		}

		if err != nil {
			switch r := errorcode.IotaError(err); r {
			case errorcode.HoldMustRequestMoreThanZeroSeats.String():
				expectedHoldSeatRequests++
			case errorcode.HoldNotEnoughFreeSeats.String():
				expectedHoldSeatRequests++
			case errorcode.HoldTimerExpired.String():
				expectedGiveUpWaitForHoldRequests++
			}
		}
	}

	// this block is only relevant if we expect all holds to expire so we know
	// that all seats should be free
	if waitBeforeQuit {
		// start a dirtyreadcounter routine
		p := dirtyreadcounter.New(
			readFrequency, // read frequency should be much smaller than cancelHoldTimeOut
			cb.QuitReceiveCh,
			cb.SeatCountSendCh,
			cb.FreeSeatsReceiveCh,
		)
		// if we wait twice the amount of time of cancel duration then all holds should be expired
		// the problem with expired holds is that we cannot count how many there were
		// because there is no location to report the cancelled request except the log
		time.Sleep(2 * cancelHoldTimeOut)

		approxSeatCounts := p.Get()
		expectedSeatCounts := dirtyreadcounter.ApproxSeatCounts{
			Free:     uint(rows * seatsPerRow), // all seats should be free because we waited long enough for holds to expire
			Held:     0,
			Reserved: 0,
		}

		if !reflect.DeepEqual(approxSeatCounts, expectedSeatCounts) {
			t.Errorf("Actual seat counts is: \n %+v  \n different than expected seat counts \n %+v ", approxSeatCounts, expectedSeatCounts)
		}
	}

	//make sure all remaining work winds down
	time.Sleep(cancelHoldTimeOut)
	//all routines must exit, this can only be called from the same location that called seatkeeper.New()
	close(cb.QuitSendCh)

	// this is executed if we do not expect all holds to expire
	if !waitBeforeQuit {
		actualStat := cb.ShowInternalStats()
		expectedStat := venue.InternalStats{
			SeatCountRequests:   0,
			HoldSeatRequests:    expectedHoldSeatRequests,
			ReserveSeatRequests: 0,
			//CancelHoldSeatRequests:    0,
			GiveUpWaitForHoldRequests: expectedGiveUpWaitForHoldRequests,
		}
		if !reflect.DeepEqual(actualStat, expectedStat) {
			t.Errorf("Actual stats is: \n %+v  \n different than expected stats \n %+v ", actualStat, expectedStat)
		}
	}
}

// the hold request counter should be similar to the size of the hold request channel
// there should be no hold requests that were abandoned;
// {SeatCountRequests:0 HoldSeatRequests:6 ReserveSeatRequests:0 CancelHoldSeatRequests:0 GiveUpWaitForHoldRequests:0}
func TestShouldInteractWithHoldFillers1(t *testing.T) {
	// wait for seatkeeper to receive hold request from the channel
	// channel size = 5, long cancel hold timeout
	helperHoldTest(t, 10*time.Millisecond, 5, 200*time.Millisecond, false, 200*time.Millisecond)
}

// the hold request counter should be similar to the size of the hold request channel
// most of the routines should give up waiting for hold requests
//{SeatCountRequests:0 HoldSeatRequests:1 ReserveSeatRequests:0 CancelHoldSeatRequests:0 GiveUpWaitForHoldRequests:4}
func TestShouldInteractWithHoldFillers2(t *testing.T) {
	// very short wait for seatkeeper to receive hold request from the channel
	// channel size = 5, long cancel hold timeout
	helperHoldTest(t, 5*time.Microsecond, 5, 200*time.Millisecond, false, 200*time.Millisecond)
}

// the number of hold requests should be very low and equal to the size of the channel
//{SeatCountRequests:0 HoldSeatRequests:2 ReserveSeatRequests:0 CancelHoldSeatRequests:0 GiveUpWaitForHoldRequests:0}
func TestShouldInteractWithHoldFillers3(t *testing.T) {
	// wait for seatkeeper to receive hold request from the channel
	// channel size = 1, long cancel hold timeout
	helperHoldTest(t, 1*time.Millisecond, 1, 200*time.Millisecond, false, 200*time.Millisecond)
}

// the expected seat counts are
// {Free:15 Held:0 Reserved:0}
func TestShouldInteractWithHoldFillers4(t *testing.T) {
	// wait for seatkeeper to receive hold request from the channel
	// channel size = 5, shorter cancel hold timeout with wait for all holds to expire
	helperHoldTest(t, 10*time.Millisecond, 5, 100*time.Millisecond, true, 10*time.Millisecond)
}
