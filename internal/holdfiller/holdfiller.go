// Package holdfiller sends and receives messages from the seatkeeper goroutine to place holds on seats.
// Incoming webrequests are already running as goroutines so there is no need to start a goroutine in this module.
package holdfiller

import (
	"context"
	"log"
	"ticket-service/internal/errorcode"
	"ticket-service/internal/seatkeeper"
	"ticket-service/internal/venue"
	"time"
)

// New creates a new hold request
// The channels that are not explicitly closed are handled by the garbage collector
func New(
	requestedSeats uint,
	customerEmail string,
	waitForHold time.Duration, // e.g. 50 * time.Millisecond
	quit seatkeeper.QuitReceiveCh,
	holdReqCh seatkeeper.HoldRequestSendCh,
) (
	venue.HoldID,
	error,
) {
	// seatkeeper must create a permanent channel buffered with size >= 1 to receive structs of channels
	// the struct can be created by a seatkeeper function
	// once the message is placed into the channel, holdfiller should wait for reply
	// before the reply message is placed in the hold results channel,
	// holdfiller can cancel and return to the user after context timeout
	//
	//											holdfiller sends hold request into unblocking channel
	//											the size of the channel needs to be determined
	// seatkeeper receives hold request
	//
	// seatkeeper								holdfiller
	// 		select									select
	//			giveupWaitForHold,1 (rec)				timer (rec)
	// 				fall out logic							giveupWaitForHold,1 (send)
	//			startinghold,0 (send)							fall out logic
	//				issue venue hold function call		startinghold,0 (rec)
	//				holdresults,1 (send)					holdresults,1 (rec)
	//
	//
	//
	// startinghold channel ensures that seatkeeper does not attempt to make a hold of venue seats
	// while holdfiller gave up and will never receive the hold results
	//

	holdReplyDetailsCh, giveUpWaitHoldCh, startingHold := seatkeeper.NewHoldChs()

	holdReqDetail := seatkeeper.HoldRequestDetails{
		CustomerEmail:           customerEmail,
		RequestedSeats:          requestedSeats,
		HoldReplyDetailsCh:      holdReplyDetailsCh,
		GiveUpWaitHoldRequestCh: giveUpWaitHoldCh,
		StartingHoldCh:          startingHold,
	}

	// try to send the hold request, if the channel is full then exit this function
	select {
	// send details of the hold request
	case holdReqCh <- holdReqDetail:
	case <-quit:
		log.Println("holdfiller: Exiting holdfiller in select 1")
		return 0, nil
	default:
		log.Println("holdfiller: hold request channel full")
		return 0, errorcode.New(errorcode.HoldChannelFull, errorcode.HoldChannelFull.String())
	}

	// start a context timer channel that expires after a given duration
	// this is how long we wait for reply from the seatkeeper
	ctx, cancel := context.WithTimeout(context.Background(), waitForHold)
	defer cancel()

	//blocking select until I can receive on one of the unbuffered channels
	select {
	case <-quit:
		log.Println("holdfiller: Exiting holdfiller in select 2")
		return 0, nil
	case <-startingHold:
		log.Println("holdfiller: Holdfiller notified that seatkeeper is starting to work on a hold")
		holdDetails := <-holdReplyDetailsCh
		return holdDetails.HoldID, holdDetails.Err
	case <-ctx.Done():
		log.Println("holdfiller: Holdfiller timer expired")
		// send empty struct on the giveUpWaitHoldCh channel (unbuffered),
		// but we want to exit immediately so the send is done in a goroutine
		// if holdfiller.New() function exits after sending the message on non-blocking channel,
		//the channel could be garbage collected before seatkeeper reads the message
		go func(
			quit seatkeeper.QuitReceiveCh,
			giveUpWaitHoldCh seatkeeper.GiveUpWaitHoldRequestSendCh,
		) {
			select {
			case <-quit:
				log.Println("holdfiller: Exiting holdfiller goroutine in select 3 ")
				return
			case giveUpWaitHoldCh <- struct{}{}:
				log.Println("holdfiller: Sending message on giveUpWaitHoldCh")
				return
			}
		}(quit, giveUpWaitHoldCh)

		return 0, errorcode.New(errorcode.HoldTimerExpired, errorcode.HoldTimerExpired.String())
	}
}
