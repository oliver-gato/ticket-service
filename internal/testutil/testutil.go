// Package testutil contains various common functions that help with unit tests
package testutil

import (
	"bytes"
	"flag"
	"io/ioutil"
	"path/filepath"
	"testing"
)

// updateGolden is a package-level global variable
var updateGolden = flag.Bool("update", false, "update .golden files")

//HelperForTest is using in unit tests
func HelperForTest(
	testName string,
	t *testing.T,
	commandToProduceGolden string,
	actualString []byte,
	fileSuffix string,
) {
	golden := filepath.Join("./testdata/", testName+fileSuffix+".golden")

	if *updateGolden {
		ioutil.WriteFile(golden, actualString, 0644)
	}
	expectedString, err := ioutil.ReadFile(golden)
	if err != nil {
		t.Errorf("Error reading golden file: %s", err.Error())
		t.Errorf(commandToProduceGolden)
	}
	if !bytes.Equal(actualString, expectedString) {
		t.Errorf("Actual %s is: \n %s \n different than expected \n %s ", fileSuffix, actualString, expectedString)
		t.Errorf(commandToProduceGolden)
	}
}
